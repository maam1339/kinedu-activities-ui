#Kinedu Activities UI

User interface to keep track of the activities that babies nursery school care.

## Download and run the application

```
git clone https://maam1339@bitbucket.org/maam1339/kinedu-activities-ui.git
```

```
cd kinedu-activities-ui
```

Install dependencies

```
npm install
```

Run the server

```
npm start
```

## Credits 

[Armando Alejandre](http://armando-alejandre.herokuapp.com/)

# Enjoy friends!

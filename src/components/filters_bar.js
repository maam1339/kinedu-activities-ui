import React from 'react';
import {Row, Input, Col} from 'react-materialize'

const FiltersBar = ({babies, assistants}) => {
  let babyOptions = babies.map( baby => {
    return <option key={baby.id} value={baby.name}>{baby.name}</option>
  });

  let assistantOptions = assistants.map( assistant => {
    return <option key={assistant.id} value={assistant.name}>{assistant.name}</option>
  });

  return (
    <Row>
      <Col s={12} className='grid-example'>
        <Input s={6} type='select' label="Bebés" defaultValue='2'>
          <option key={0} value="">Selecciona un bebé</option>
          {babyOptions}
        </Input>

        <Input s={6} type='select' label="Asistentes" defaultValue='2'>
          <option key={0} value="">Seleciona un asistente</option>
          {assistantOptions}
        </Input>
      </Col>
    </Row>
  );
};

// onChange={ e => this.setState({ value: e.target.value}) }
export default FiltersBar;
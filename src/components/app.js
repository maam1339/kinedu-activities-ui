import React, { Component } from 'react';
import FiltersBar from './filters_bar'
import ActivityLogsTable from './activity_logs_table'
import { Navbar, NavItem, Row, Col, Icon} from 'react-materialize'

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      babies: [],
      assistants: [],
      activityLogs: [],
      styles: {
        margin: '50px auto',
        background: 'white'
      }
    };
  }

  async componentDidMount() {
    let babies = await fetch('http://localhost:3000/v1/babies');
    let assistants = await fetch('http://localhost:3000/v1/assistants');
    let activityLogs = await fetch('http://localhost:3000/v1/activity_logs');

    this.setState({
      babies: await babies.json(),
      assistants: await assistants.json(),
      activityLogs: await activityLogs.json(),
    });
  }

  render() {
    return (
      <div className="container" style={this.state.styles}>
        <Row>
          <Col s={12} className='grid-example'>
            <Navbar brand='Kinedu' right style={{background: 'grey'}}>
              <NavItem href='/v1/activity_logs'><Icon>refresh</Icon></NavItem>
            </Navbar>
          </Col>
        </Row>

        <FiltersBar babies={this.state.babies} assistants={this.state.assistants} />
        <ActivityLogsTable activityLogs={this.state.activityLogs} />
      </div>
    );
  }
}

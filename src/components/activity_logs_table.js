import React from 'react';
import { Table, Row, Col } from 'react-materialize'

const ActivityLogsTable = ({activityLogs}) => {
  let options = activityLogs.map( activity => {
    return (
      <tr key={activity.id}>
        <td>{activity.baby.name}</td>
        <td>{activity.assistant.name}</td>
        <td>{activity.activity.name}</td>
        <td>{activity.created_at}</td>
        <td>-</td>
        <td>{activity.duration}</td>
      </tr>
    );
  });

  return (
    <Row>
      <Col s={12} className='grid-example'>
        <Table>
          <thead>
            <tr>
              <th data-field="id">Bebé</th>
              <th data-field="name">Asistente</th>
              <th data-field="price">Actividad</th>
              <th data-field="price">Inicio</th>
              <th data-field="price">Estatus</th>
              <th data-field="price">Duración</th>
            </tr>
          </thead>

          <tbody>
            {options}
          </tbody>
        </Table>
      </Col>
    </Row>
  );
};

export default ActivityLogsTable;